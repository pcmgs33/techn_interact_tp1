package achabert.fisheye3;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.SeekBar;

import achabert.fisheye3.deform.AbstractFormula;
import achabert.fisheye3.deform.BasicDeform;
import achabert.fisheye3.images.DeformableView;


public class FishEyeView extends Activity {

    //la formule de transformation set basicDeform
    private static final AbstractFormula[] formulas = { new BasicDeform()  };
    private int currentFormula = 0;

    private DeformableView view;


    private boolean isPartiallyDeformed = true;


    //PERSO
    /*
        Sensor manager permet de lier un sensor a une action
        un sensor est juste la d�finition du sensor
    */
    SensorManager sm;
    Sensor s;
    SensorEventListener myListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fish_eye_view);

        if (savedInstanceState != null) currentFormula = savedInstanceState.getInt("currentFormula", 0);


        view = (DeformableView) findViewById(R.id.deformed);
        view.setPartiallyDeformed(isPartiallyDeformed);
        view.setDeformation(formulas[currentFormula]);
        formulas[0].setParams(100, 300, 200);

        //PERSO
          /*
        Init du sensor manager et du sensor
         */
        sm = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        s = sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        myListener = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent event) {
                float[] data  = event.values;
                float x = data[0];
                float y = data[1];

                view.centreX +=x;
                view.centreY +=y;

                view.processDeform();
                view.invalidate();
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {

            }
        };


    }


    protected void onResume() {
        super.onResume();
        /*
            On enregistre le listener perso sur le sensor s avec le delay fastest
        */
        sm.registerListener(myListener,s,SensorManager.SENSOR_DELAY_FASTEST);

    }


    /* stop en cas de quit... */
    /* il faudra relancer l'�coute en cliquant sur le bouton */
    protected void onPause() {
        super.onPause();

        /*

        On enregistre le listener perso sur le sensor s avec le delay fastest
         */
        sm.registerListener(myListener,s,SensorManager.SENSOR_DELAY_FASTEST);

    }
    protected void onStop() {
        super.onStop();

        /*
            On unregister le listener sur le sensor s
         */
        sm.unregisterListener(myListener, s);

    }

    /*
    public void launchStop(View button) {
        if (myListener.isRunning()) myListener.stop();
        else myListener.start();
    }
    */
}
